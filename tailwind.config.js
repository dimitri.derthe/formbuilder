module.exports = {
  theme: {
    extend: {
      colors: {
        primary: '#0d2a3e',
        secondary: '#9fadb4',
        light: '#edf3f5',
      },
    },
  },
}
