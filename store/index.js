import { v4 as uuidv4 } from 'uuid'

export const actions = {
  async getUid() {
    return uuidv4()
  },
  async getInputId() {
    return uuidv4().slice(0, 8)
  },
}
